import numpy as np
# ase packages
from ase.build import fcc100
from ase.visualize import view
from ase import Atoms, Atom
from ase.calculators.emt import EMT
from ase.build import add_adsorbate
from ase.optimize import QuasiNewton
from ase.constraints import FixAtoms, FixedLine
from ase.io import read, write
# plotting packages
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
# creates a 3x3x2 gold surface
system = fcc100(
    'Au',
    size=(3,3,2),
    vacuum=8.0,
)
system.center(axis=2) # centers the surface in the z-drection
# set up constraint of the movement of the gold atoms
mask = [atom.tag for atom in system]
constraint = FixAtoms(mask=mask) # fixes all the atoms in the system
# add silver adsorbate metal ontop an Au atom of the surface
add_adsorbate(
    system, # specifies to what we add the adsorbate to
    'Ag', # adsorbate species
    1.85, # distance between the atom and the adsorbate
    'ontop', # added directly on top of one of the slab atoms
)
# redefines the adsorbate (Ag)
# as the last atom in the system
Ag = system[-1]
# constraints the Ag adsorbate metals movement to be in the z direction
line = FixedLine(
    Ag.index, # index of the Ag atom in the system
    (0, 0, 1), # only allow movement in z direction (x, y, z)
)
system.set_constraint( # imposes our constraints to the system
    (
    constraint, # slab constraints
    line, # adsorbate constraint
    )
)
system.set_calculator(EMT()) # sets calculator to EMT
# storage lists
x = []
y = []
E = []
n = 50 # number of steps in each direction
step = system.get_cell()[0,0]/float(n+1) # step size for each iteration
x_ini = Ag.x # initial x-position

dir = 'trajs/' # directory for saving trajectories

for _ in range(n): # loops through all y positions
    # adds an empty list to the end of each list variable
    x.append([])
    y.append([])
    E.append([])
    for _ in range(n): # loops through all x positions
        # sets up force minimization
        qn = QuasiNewton(
            system,
            trajectory=dir + 'x_%1.1f_y_%1.1f.traj' % (Ag.x, Ag.y),
        )
        qn.run(fmax=0.01) # runs optimization
        # appends results
        E[-1].append(system.get_potential_energy())
        y[-1].append(Ag.y)
        x[-1].append(Ag.x)
        Ag.x += step
    Ag.x = x_ini # moves Ag back to original x-position
    Ag.y += step # move in y direction
# plots the results
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(
    np.array(x), np.array(y), np.array(E),
    cmap=cm.coolwarm,
)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
