from ase import Atom, Atoms
# first way
d = 1.8
system = Atoms(
    (
    Atom('N', position=(0.0, 0.0, 0.0)),
    Atom('N', position=(0.0, 0.0, d)),
    )
)
from ase.visualize import view
#view(system) # uncomment to view the system

# another way
system = Atoms() # empty atoms
for i in range(2):
    system.extend(Atom('N', position=(0.0, 0.0, i*d)))

# yet another way
system = Atoms(
    symbols='N2',
    positions = ((0.0, 0.0, 0.0), (0.0, 0.0, d)),
    pbc=False,
)

# you can even copy an existing system
system2 = system.copy()

# there are many properties that may retrieved from a system via the
# Atom object's methods
print("The position of the atoms:")
print(system.get_positions())

from ase.build import fcc100
# there are many predefined structures in ASE, so you do not have to
# make up one from scratch, for instance a face-centered 100 crystal
system = fcc100(
    'Cu',
    size=(3,3,2),
    vacuum=8.0,
)
# view(system) # uncomment to view
# you can directly manipulate a system using ASE
# here is an example of moving the 4th Cu atom [3] -2 Å in the z-direction [2]
system.positions[3][2] -= 2.0
# view(system)
# you can see the chemical composition of the system too
print(system.get_chemical_symbols())
# you can also change an atoms species
system.arrays['numbers'][3] = 79
# view(system)
# you can also save your systems/data and read files
from ase.io import write, read
write('system.traj', system) # saves the system as a trajectory file
system_from_file = read('system.traj')
#view(system_from_file)

# There are too many applications and types of ASE simulations to list,
# but what is common amongst them all is that you attach a "calculator" to
# your system. A multitude of calculators exist, and are often specialized
# towards certain problems.
# In this class we will primarily use a DFT solver/calculator called NWChem,
# which calculates the electronic structure of a given system.
from ase.calculators.nwchem import NWChem
# configure the calculator
calculator = NWChem(
    label='calc/nwchem', # where nwchem files are stored
    maxiter=2000,
    xc='B3LYP', # which exchange correlation function to use
    basis='6-31+G*', # which basis set to use
)
system.set_calculator(calculator) # attaches calculator
# Using this calculator we can for instance optimize the systems structure
# by using a force minimizer like QuasiNewton
from ase.optimize import QuasiNewton
qn = QuasiNewton(
    system, # system to minimize
    trajectory='trajs/n2.traj', # saves the structure optimization as specified
)
qn.run(fmax=0.01) # performs optimization within a maximum allows force
