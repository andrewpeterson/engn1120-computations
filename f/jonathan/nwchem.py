# use these settings for the H2
calculator = NWChem(
    label='calc/nwchem', # where nwchem files are stored
    maxiter=2000,
    xc='B3LYP', # which exchange correlation function to use
    basis='6-31+G*', # which basis set to use
)

# use these settings for the OH
calc = NWChem(
    label='calc/nwchem',
    maxiter=2000,
    xc='B3LYP',
    basis='6-31+G**',
    odft=True, # open shell dft is true
    mult=2, # ground state is a doublet 
)
