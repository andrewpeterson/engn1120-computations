import numpy as np
from scipy.optimize import fsolve
from scipy.integrate import odeint
from matplotlib import pyplot


def get_jacobian(yss):
    """Returns the Jacobian at a given steady-state solution."""
    y1, y2 = yss
    J = np.array([[2. * y1 + 1, -1.],
                  [-1., -6. * y2 + 2.]])
    return J


def get_dydt(y, t=None):
    """Returns the original differential equations."""
    y1, y2 = y
    dy1dt = y1**2 + y1 - y2 - 1.
    dy2dt = - y1 - 3. * y2**2 + 2. * y2 + 2.
    return [dy1dt, dy2dt]


# First we'll examine the root around (1, 1).
yss = fsolve(func=get_dydt, x0=(1., 1.))
J = get_jacobian(yss)
eigs = np.linalg.eig(J)
print('The eigenvalues for yss={} are {}.'
      .format(str(yss), str(eigs[0])))

# It's unstable. We'll now numerically integrate to check.
fig, axes = pyplot.subplots(ncols=2, figsize=(6.5, 3.))
fig.subplots_adjust(left=0.08, right=0.99, bottom=0.13, wspace=0.25)
times = np.linspace(0., 10., num=100)
epsilons = [(0.1, 0.1), (-0.1, 0.1)]
for epsilon, ax in zip(epsilons, axes):
    ans = odeint(func=get_dydt, y0=yss + epsilon, t=times)
    ax.plot(times, ans)
    ax.set_title(r'From $\epsilon$  = ' + '({:.1f}, {:.1f})'.
                 format(epsilon[0], epsilon[1]))
    ax.set_ylabel('$y_1$, $y_2$')
    ax.set_xlabel('$t$')
fig.savefig('out.pdf')
