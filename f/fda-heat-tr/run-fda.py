import numpy as np
from scipy.optimize import root
from matplotlib import pyplot

def get_f(y, t=None):
    T = y  # un-pack variables
    f = np.empty(len(T))  # Will hold functions.
    # Inside boundary condition.
    f[0] = T[0] - Tw
    # Interior points.
    for i in range(1, n-1):
        f[i] = 0.5 * (T[i+1] - T[i-1]) + \
               (rA / dr + i) * (T[i+1] - 2. * T[i] + T[i-1])
    # Outside boundary condition.
    Tn = T[n - 2] - 2. * h * dr / k * (T[n - 1] - T_amb)
    i = n - 1
    f[i] = 0.5 * (Tn - T[i-1]) + \
        (rA / dr + i) * (Tn - 2. * T[i] + T[i-1])
    return f

# Known constants.
rA = 0.03  # m, inner radius
rB = 0.06  # m, outer radius
Tw = 400.  # K, wall temperature
h = 10.  # W/m2/K
k = 20.  # W/m/K
T_amb = 298.  # K, ambient temperature

# Solution-specific numbers.
n = 50  # Number of discretizing opints.
dr = (rB - rA) / n  # Grid spacing.
guess = np.linspace(Tw, T_amb, num=n)
roots = root(fun=get_f, x0=guess)
profile = roots.x

# Plot.
fig, ax = pyplot.subplots(figsize=(3., 3.))
fig.subplots_adjust(left=0.25, bottom=0.15, right=0.95, top=0.95)
rs = np.linspace(rA, rB, num=n)
ax.plot(rs, profile)
ax.set_xlabel('distance, m')
ax.set_ylabel('temperature, K')
fig.savefig('out-fda.pdf')
