import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot

def get_f(y, t=None):
    T = y  # un-pack variables
    f = np.empty(len(T))  # Will hold functions.
    # Inside boundary condition.
    f[0] = 0.
    # Interior points.
    for i in range(1, n-1):
        r = rA + i * dr
        f[i] = (k / rho / Cp * (1. / r * (T[i+1] - T[i-1]) / 2. / dr
                                + (T[i+1] - 2. * T[i] + T[i-1]) / dr**2))
    # Outside boundary condition.
    Tn = T[n - 2] - 2. * h * dr / k * (T[n - 1] - T_amb)
    i = n - 1
    r = rB
    f[i] = (k / rho / Cp * (1. / r * (Tn - T[i-1]) / 2. / dr
                            + (Tn - 2. * T[i] + T[i-1]) / dr**2))
    return f

# Known constants.
rA = 0.03  # m, inner radius
rB = 0.06  # m, outer radius
Tw = 400.  # K, wall temperature
h = 10.  # W/m2/K
k = 20.  # W/m/K
T_amb = 298.  # K, ambient temperature
rho = 7800.  # kg/m^3, density
Cp = 460.  # J/kg/K, heat capacity

# Solution-specific numbers.
n = 50  # Number of discretizing opints.
dr = (rB - rA) / n  # Grid spacing.
initial_profile = np.linspace(T_amb, T_amb, num=n)
initial_profile[0] = Tw  # Set the Dirichlet boundary condition.
times = np.linspace(0., 200., num=10)
ans = odeint(func=get_f, y0=initial_profile, t=times)

# Plot.
fig, ax = pyplot.subplots(figsize=(3., 3.))
fig.subplots_adjust(left=0.25, bottom=0.15, right=0.95, top=0.95)
rs = np.linspace(rA, rB, num=n)
for time, profile in zip(times, ans):
    ax.plot(rs, profile, '0.5')
ax.set_xlabel('distance, m')
ax.set_ylabel('temperature, K')
fig.savefig('out-mol.pdf')
