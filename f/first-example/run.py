"""
Script to create a plot of a function.
"""
import numpy
from matplotlib import pyplot

def get_f1(x):
    """Returns the value of f1 at x."""
    f1 = .1 * x**2 + 2. * numpy.sin(3. * x)
    return f1

xs = numpy.linspace(3., 15., num=100)
ys = [get_f1(x) for x in xs]

fig, ax = pyplot.subplots()
ax.plot(xs, ys)
ax.set_xlabel('$x$')
ax.set_ylabel('$f_1$')
fig.savefig('out.pdf')
