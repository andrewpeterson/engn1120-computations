import numpy as np
from matplotlib import pyplot

def true_function(x):
    a = .3
    b = 2.
    c = 9.
    return a * x**2 + b * x + c


# Look at the true curve.
xs = np.linspace(-10., 5.)
ys = np.array([true_function(_) for _ in xs])
fig, ax = pyplot.subplots()
ax.plot(xs, ys)
fig.savefig('make-data.pdf')

# Generate randomly scattered data about this curve.
random = np.random.RandomState(seed=42)
x_data = np.array([-7.5, -4., -1.5, 2., 4.])
x_data += random.normal(scale=0.5, size=x_data.shape)
y_data = np.array([true_function(_) for _ in x_data])
y_data += random.normal(scale=1.0, size=y_data.shape)
ax.plot(x_data, y_data, 'ro')
fig.savefig('make-data.pdf')

# Write out the data suitable for latex.
def w(text):
    print(text)
    f.write(text + '\n')

f = open('data.tex', 'w')
w(r'\begin{tabular}{rr}')
w(r'\hline \hline')
w(r'$x$ & $y$ \\')
w(r'\hline')
for x, y in zip(x_data, y_data):
    w(r'{:.3f} & {:.3f} \\'.format(x, y))
w(r'\hline \hline')
w(r'\end{tabular}')
f.close()

# Write out data suitable for python.
f = open('data.txt', 'w')
for x, y in zip(x_data, y_data):
    f.write('{:.3f} {:.3f}\n'.format(x, y))
f.close()

