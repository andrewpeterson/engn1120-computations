import numpy as np
from scipy.optimize import fmin
from matplotlib import pyplot

xs = [ -7.252, -4.069, -1.176, 2.762, 3.883]
ys = [ 10.038, 7.408, 7.830, 16.341, 21.832]

def get_predicted_y(x, a, b, c):
    return a * x**2 + b * x + c

def get_loss_function(p):
    loss = 0.
    for x, y in zip(xs, ys):
        y_pred = get_predicted_y(x, p[0], p[1], p[2])
        loss += (y - y_pred)**2
    print(p, loss)
    return loss

ans = fmin(func=get_loss_function, x0=[1., 1., 1.])

# Plot the results.
fig, ax = pyplot.subplots()
xplots = np.linspace(min(xs), max(xs))
yplots = [get_predicted_y(x, ans[0], ans[1], ans[2]) for x in xplots] 
ax.plot(xplots, yplots, 'b-')
ax.plot(xs, ys, 'ro')
ax.text(-6., 16., 'a, b, c = {:.3f}, {:.3f}, {:.3f}'.format(ans[0], ans[1], ans[2]))
ax.set_xlabel('$x$')
ax.set_ylabel('$y$')
fig.savefig('out.pdf')
