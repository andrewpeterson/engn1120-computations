\documentclass[12pt]{article}

% let's use some nice packages
\usepackage{times}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[margin=2.5cm]{geometry}
\usepackage[format=hang,font=small,labelfont=bf]{caption}
\usepackage{color}
\usepackage{pdfcolmk} % allow colors to span pages
% for crossing out terms in equations
\usepackage[makeroom]{cancel}
% attach file with \attachfile{filename}{comments...}
\usepackage[hidelinks]{attachfile}
% for python and other code highlighting
\usepackage{minted} % need pdflatex --shell-escape
%\newmintedfile[pythoncode]{python}{linenos=true}
\usepackage{upquote} % good quotes in minted
\usepackage{graphicx}
\newmintedfile[pythoncode]{python}{linenos=true,fontsize=\footnotesize}
\usepackage{soul}
\usepackage{enumitem}
\usepackage[framemethod=TikZ]{mdframed}
\usepackage{mhchem}

% for fancy and easy modification of header and footer
\usepackage{fancyhdr}
\fancyhf{} % sets both header and footer to nothing
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.1pt}
\pagestyle{fancy}
\lfoot{\thepage}
\cfoot{}
\rfoot{\scriptsize \textcopyright\ A. Peterson, Brown University. Unauthorized distribution prohibited.}

% Shortcuts.
\newcommand{\mip}[1]{\mintinline{python}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Submission box
\newenvironment{submission}{%
\mdfsetup{%
frametitle={%
\tikz[baseline=(current bounding box.east),outer sep=0pt]
\node[anchor=east,rectangle,fill=blue!20]
{\strut Submission};}}%
%
\mdfsetup{innertopmargin=2pt,linecolor=blue!20,%
linewidth=2pt,topline=true,%
frametitleaboveskip=\dimexpr-\ht\strutbox\relax
}
\begin{mdframed}[leftmargin=1in, rightmargin=1in]\relax%
}{\end{mdframed}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Paper submission box
\newenvironment{papersubmission}{%
\mdfsetup{%
frametitle={%
\tikz[baseline=(current bounding box.east),outer sep=0pt]
\node[anchor=east,rectangle,fill=red!20]
{\strut Paper Submission};}}%
%
\mdfsetup{innertopmargin=2pt,linecolor=red!20,%
linewidth=2pt,topline=true,%
frametitleaboveskip=\dimexpr-\ht\strutbox\relax
}
\begin{mdframed}[leftmargin=1in, rightmargin=1in]\relax%
}{\end{mdframed}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Python code (pcode)
\newcommand{\pcode}[1]{
\noindent
{\footnotesize File attached here.  \attachfile{#1}
}
\pythoncode{#1}
}


%% Comment commands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\fu}[1]{\textbf{\color{red}#1\normalcolor}}
%\renewcommand{\fu}[1]{}

%% Title and authors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{{\small ENGN 1120:}\\ Computational Methods}
\author{Brown University School of Engineering}


\begin{document}



% show the title, author and date
\maketitle
\thispagestyle{fancy}

\noindent
A modern course in reaction kinetics relies heavily on scientific computing, as problems that are not easily solvable ``by hand'' tend to be the rule, not the exception.
In this class, our examples and homework solutions will be provided using python.
You are free to use any software of your choosing, however.
This tutorial goes over some basics.

\tableofcontents

\section{Reading this tutorial}

This tutorial is a living document that will change throughout the semester.
You can find the latest version at the repository given below.\\
\emph{Repository:}
{\footnotesize \url{https://bitbucket.org/andrewpeterson/engn1120-computations} }
\\
\emph{Direct link:}
{\footnotesize \url{https://bitbucket.org/andrewpeterson/engn1120-computations/raw/master/computational-methods.pdf} }

\section{Python}

\subsection{Installing python}

You can use python in the computer lab, or from your own computer via any installation method you prefer.
Two simple solutions are:
\begin{itemize}
\item Anaconda: a ``one-click'' installer for Windows, Mac, Linux.\\ \url{https://www.anaconda.com/download}
\item Pythonanywhere: a web interface to a terminal that runs python.\\ \url{https://www.pythonanywhere.com}
\end{itemize}

\noindent
In this class, we will use version Python 3.6.
Make sure you have the following packages installed, which give you roughly the same commands as Matlab:

\begin{itemize}[noitemsep]
\item  numpy: (numeric python) for creating matrixes, vectors, etc.
\item scipy: (scientific python) for scientific computing, linear algebra, optimization, etc.
\item matplotlib: (matrix plotting library) to create high-quality figures
\end{itemize}

\noindent
You can verify they are installed by opening a python console (in pythonanywhere, open a Bash terminal and type `ipython3') then typing the commands at the prompts.
If they run without errors, your installation works, and you can exit the python interpreter by typing \verb'exit()'.

\begin{minted}[fontsize=\footnotesize]{python}
>>> import numpy
>>> import scipy
>>> import matplotlib
\end{minted}

\subsection{Running python}

As an open-source project that enjoys immense popularity, there are many different ways to run python.
For example, \textit{Spyder} is an interface meant to look like Matlab.
\textit{Jupyter} is a notebook-like interface (a bit like Mathematica) that you run locally in your web browser.
However, a python script is just a text file, \textit{e.g.}, called ``run.py'', that you can execute by typing \mintinline{bash}{python run.py} at a command line.
Any of these are fine options; here for simplicity we'll tend to assume you are using a plain text file.

If you'd like a prompt where you can type python commands, just type \mintinline{bash}{python} (or for a more colorful version type \mintinline{bash}{ipython}).


\subsection{A first example}

It's easiest to see the basic concepts if we have example script.
The below script produces a simple plot (Figure~\ref{fig:first-example}).
Note that you should be able to directly extract the script, as a text file, from this document as an attachment.

\noindent
File attached here.  \attachfile{f/first-example/run.py}
\pythoncode{f/first-example/run.py}


\begin{figure}
\centering
\includegraphics[width=0.6 \textwidth]{f/first-example/out.pdf}
\caption{The figure produced from an example script.
\label{fig:first-example}
}
\end{figure}

We can note several things:
\begin{itemize}
\item \emph{Lines 4--5:} Compared to matlab, python has almost no ``built-in'' commands---that is, a very clean namespace. Any specialized command that you want to use needs to be imported, and these import lines typically start off the script.
\item \emph{Lines 7--10:} This defines a function; these functions can be in the same text file as your script, and by convention go at the top. A function starts with \mintinline{python}{def} and ends with \mintinline{python}{return}. This function is named \mintinline{python}{get_f1}, takes in the variable \mintinline{python}{x} and returns the variable \mintinline{python}{f1}.
\item \emph{Line 9:} Note that the function for $\sin$ is contained in numpy. Numpy is where you'll find most of your basic math, including arrays.
\item \emph{Line 11:} \mintinline{python}{linspace} is a command to create a linearly-spaced numpy array.
\item \emph{Line 12:} The part in the square brackets is a shortcut to make an array. This is equivalent to writing:
\begin{minted}[fontsize=\footnotesize]{python}
ys = []  # An empty list.
for x in xs:
    y = get_f1(x)
    ys.append(y)
\end{minted}
\item \emph{Line 15:} This is a command to start making plots. Note that \mip{fig} and \mip{ax} are both \emph{objects}. \mip{fig} is the figure (basically like a page) and \mip{ax} is the axes object where the plot is made; you can have multiple axes objects on a single figure.
\item \emph{Lines 16--19:} \mip{fig} and \mip{ax} have methods that can be called; these should look similar to Matlab commands.
\item \emph{Line 17:} Strings of text can be contained in either single (\mip{'}) or double (\mip{"}) quotes. Note that pyplot supports \LaTeX\ commands denoted with dollar signs; this is how the subscript was generated (and why the characters are italicized).
\end{itemize}

\subsection{Getting help}

It can be convenient to get help right inside of python.
From a python prompt, try:

\begin{minted}[fontsize=\footnotesize]{python}
>>> help(ax)  # Displays documentation for command ax.
>>> dir(ax)  # Shows all the methods contained in ax.
\end{minted}

\section{The Atomic Simulation Environment (ASE)}

The Atomic Simulation Environment (ASE) provides powerful tools for working with atomic structures.
Below you will find an example of some basic operations you can do with ASE, such as building and manipulating an atomic system, basic i/o, attaching calculators and running a structure optimization.
\noindent
File attached here.  \attachfile{f/jonathan/ase_basics.py}
\pythoncode{f/jonathan/ase_basics.py}

% I made this example problem, which is essentially the first homework you gave
% us in 2770. I thought it was a pretty nice a simple problem that shows
% a lot of the features of ASE, but it may be too similar to any homeworks
% you have planned, so do review the code before you uncomment this

%\section{ASE Example Problem}
%Below is an example of using ASE to find the potential energy surface of a silver atom adsorbing onto a fixed gold fcc100 slab.
%\noindent
%File attached here.  \attachfile{f/jonathan/example.py}
%\pythoncode{f/jonathan/example.py}

\subsection{Electronic structure calculators}

We can think of atomic simulations as having two parts: the first is dictating how atoms move in response to the forces on each atom.
We can call this ``atomistics'', and most of the routines to handle this are included in ASE directly.
The second is the calculation of the electronic structure (that is, the electronic orbitals for a given atomic geometry); this in turn dictates the force on each atom and is therefore the input to the atomistic routines above.

In ASE, the electronic structure calculator is called the ``calculator'', and this can utilize (at least 20) external programs that solve the electronic structure problem.
In our examples, we'll primarily rely on ``NWChem'', which is an open-source calculator produced at the Pacific Northwest National Laboratory.

\subsubsection{NWChem settings}

Use the following NWChem settings for the H-H and O-H molecules respectively.
\noindent
File attached here.  \attachfile{f/jonathan/nwchem.py}
\pythoncode{f/jonathan/nwchem.py}

\section{Brown's Center for Computation and Visualization (CCV)}

We'll use the CCV, which is Brown's high-performance computational center, for certain electronic structure problems.
There are two primary reasons to use such a center: (1) we can run more computationally expensive calculations than we would want to on our own computers, and (2) we can avoid needing to install specialized software, like NWChem, on our own machines.

\subsection{Connecting }

You connect to the CCV using SSH.
For Windows users, this means using Putty, enter the host address: \textbf{ssh3.ccv.brown.edu} and connect using your CCV guest username and password.
Mac and Linux users can use the Terminal application, which comes built-in to connect via the ssh command (replace guestx with your guest account number):

\begin{minted}[fontsize=\footnotesize]{bash}
ssh guestx@ssh3.ccv.brown.edu
\end{minted}

If you wish to have access to plotting and graphical applications the "-Y" flag should be added to the ssh command:

\begin{minted}[fontsize=\footnotesize]{bash}
ssh -Y guestx@ssh3.ccv.brown.edu
\end{minted}
Keep in mind that you may have to install a software called X11 (XQuartz on Mac) to use this feature.
\href{https://web1.ccv.brown.edu/doc}{\textbf{Learn more about the CCV here}}\par

\subsection{Basic Commands}

The CCV uses a command line interface (CLI), and inside you will need a few basic commands to get around.
\begin{description}
  \item[pwd] Prints working directory, i.e. where you are.
  \item[cd] Changes directory, "cd destination", type "cd .." to be back one directory
  \item[ls] Lists the files and folders.
  \item[mv] Moves or renames a file, "mv from\_here to\_here"
  \item[cp] Copies a file "cp original copy"
  \item[mkdir] Creates a new directory "mkdir name\_of\_folder"
  \item[touch] Creates a new file "touch name.ext"
  \item[rm] Removes a file "rm file\_to\_be\_removed", add "-R" flag to remove recursively
  \item[cat] Displays the content of a file "cat file"
\end{description}
\noindent\href{https://code.snipcademy.com/tutorials/linux-command-line}{\textbf{Learn more about the CLI here}}\par

\noindent
To view and edit files in the CCV, you can use one of the built-in text editors.
For beginners, using \textbf{nano} or \textbf{gedit} may be best.
Advanced users tend towards \textbf{vim} or \textbf{emacs}.


\subsection{Submitting Jobs}

To start using the CCV for ASE and NWChem computation, you need to copy the '.modules' file from Jonathan's home directory.
\begin{minted}[fontsize=\footnotesize]{bash}
cp /users/guest101/.modules ~
\end{minted}

\noindent
From your own home directory (you can always get back by just typing cd), use the following command afterwards:
\begin{minted}[fontsize=\footnotesize]{bash}
source .modules
\end{minted}

This will load two commands that you need to run once everytime you log onto the CCV and want to run ASE or NWChem.
The commands are \textbf{loadase} and \textbf{loadnwchem}, what these do is pretty self explanatory, but it is worth knowing that \textbf{loadnwchem} also does \textbf{loadase}, so you do not need to do both.

To run code on the CCV, you must submit a job to the cluster, which is done using the sbatch command.
To submit the job ``file.py'' to the cluster, use
\begin{minted}[fontsize=\footnotesize]{bash}
sbatch file.py
\end{minted}

\noindent
If successful, the system will prompt you that your job has been submitted.
You can view which jobs are current submitted and/or running by using the \textbf{myq} command.
Canceling jobs is done via \mintinline{bash}{scancel <jobid>}, where \mintinline{bash}{<jobid>} is the ID number of the job that you can find with myq.
If a job completes (fails or succeeds) a \textbf{slurm-job\_id.out} file is placed in the directory you submitted the job in.

Every .py file that you wish to submit to the cluster needs to have header comments, giving special instructions to the CCV, here is one example:

\noindent
File attached here.  \attachfile{f/jonathan/sbatch.py}
\pythoncode{f/jonathan/sbatch.py}

\begin{itemize}
\item \emph{Line 1:} Specifies which software to run the file; you typically will use this line exactly like in the example.
\item \emph{Line 2:} Time the CCV will allocate to your job before killing it; this shows 1 hour.
\item \emph{Line 3:} Number of nodes you want; you typically should not ask for more than 1.
\item \emph{Line 4:} Number of tasks per node; this is the number of cores that will run your job if it runs in parallel.
\item \emph{Line 5:} The partition to run on; don't touch this line.
\item \emph{Line 6:} How much memory you need.
\end{itemize}


\section{Numerical methods}

\subsection{Numerical integration \label{sec:num-ode}}

A common problem we will encounter in this problem is the need to numerically integrate a set of ordinary differential equations (ODEs).
Imagine that in the course of solving a problem you create the following pair of ODEs:

\begin{equation} \label{eq:numerical-integrate}
	\frac{d}{dt}
\left[ \begin{array}{c}
	y_1 \\ y_2
\end{array} \right] =
\left[ \begin{array}{c}
	y_2 - 10 e^{-t} \\
	y_1 + 10 \sin 10 t \\
\end{array} \right]
\end{equation}

\noindent
We can numerically integrate this.
A numerical integrator named \mintinline{python}{odeint} exists in scipy, and we show an example of integrating from an initial value of $y_1 = 5$, $y_2 = 2$ for 2 seconds below.
The resulting plot is shown in Figure~\ref{fig:numerical-integrate}.

\noindent
File attached here.  \attachfile{f/numerical-integrate/run.py}
\pythoncode{f/numerical-integrate/run.py}


\begin{figure}
\centering
\includegraphics[width=0.7 \textwidth]{f/numerical-integrate/out.pdf}
\caption{Numerical integration example.
\label{fig:numerical-integrate}
}
\end{figure}

\noindent
Notes:

\begin{itemize}
	\item \emph{Lines 6--10:} This defines the derivative; that is, it returns a list (or array) of numbers equal to the function on the right-hand side of eq.~\eqref{eq:numerical-integrate}.
		Note that it has to receive as arguments \verb'y' and \verb't', where \verb'y' is an array as in eq.~\eqref{eq:numerical-integrate} and \verb't' is a scalar.
		In line 7, the \verb'y' arrray is ``unpacked'' to give \verb'y1' and \verb'y2'.
		In line 10, it is re-packed to give an output array of $y'$ values.
	\item \emph{Line 13:} This array of times are the points at which the values of $y$ will be computed.
	\item \emph{Line 14:} This line is the integration routine, which uses \verb'odeint'.
		It takes in the function (\verb'get_yprime'), the times at which to evaluate the answer, and the values of $y$ at time zero.
	\item \emph{Lines 15--16:} \verb'odeint' returns an array of size (number of time points) $\times$ (number of variables), so we can extract the two time series as shown.
\end{itemize}

\subsection{(Non-linear) regression}

It is a common task to regress a model to measured data, in order to estimate parameter values such as rate constants or equilibrium constants.
First, a historical note.
Many textbooks will suggest that you first find a way to linearize your equations and to fit a straight line to the transformed data.
In general, such a linearization strategy is no longer necessary and can even be detrimental, as it may weight certain residuals heavier than others as an artifact of the linearization procedure.\footnote{However, linearization procedures can still be useful---an Arrhenius plot of $\ln k$ versus $1/T$ is a common example of a linearized form in which data will be commonly shown.
And since rate constants can vary by many orders of magnitude, it makes much more sense to minimize residuals of $\ln k$ rather than $k$; otherwise residuals of large $k$'s would hold much more weight than residuals of small $k$'s.}

Here, we'll show how to fit the parameters of a general, non-linear model to provided data.
The technique is simple: we guess parameters for our model and use it to predict our data.
We compare the predictions to our measured data through a ``cost function''---typically a sum of square residuals---and we use an optimizer to make this cost function as small as possible by varying our parameters.

Let's assume that we have the below data, and expect it to fit a parabola.
Our goal is to find the parameters that best describe the parameter, given the data available.

\begin{center}
\input{f/regression/data}
\end{center}

\noindent
Our model is that a parabola; that is, to find $a$, $b$, and $c$ in

\begin{equation}\label{eq:regression-model}
y^\mathrm{pred}(x) = a x^2 + b x + c
\end{equation}

\noindent
To do so we'll minimize a loss function that gives the `sum of square residuals' (SSR) of the model's prediction as compared to the actual data:

\begin{equation}\label{eq:regression-loss}
f = \sum_{i=1}^{N} (y^\mathrm{pred}(x_i) - y^\mathrm{meas}_i)^2
\end{equation}

\noindent
File attached here.  \attachfile{f/regression/run.py}
\pythoncode{f/regression/run.py}

\noindent
Notes:

\begin{itemize}
	\item \emph{Lines 8--9:} The ``model'' of equation~\eqref{eq:regression-model}.
	\item \emph{Lines 11--17:} The loss function (SSR) of equation~\eqref{eq:regression-loss}.
		Note that it needs to take exactly one input (here called \verb'p'), but this input can be an array.
		Here, our array contains the values of a, b, and c, which we can see are fed to the model in line 14.
		Note the \verb'+=' statement of line 15 is a shortcut, equivalent to writing \verb'loss = loss + ...'.
		The \verb'print' statement can be useful to monitor how the procedure is behaving.

	\item \emph{Line 19:} The regression itself occurs with a single command, \verb'fmin'.
		This takes as arguments the function to be minimized and an initial guess of the best parameters.
\end{itemize}

The result of this script is Figure~\ref{fig:regression}.

\begin{figure}
\centering
\includegraphics[width=0.6 \textwidth]{f/regression/out.pdf}
\caption{Regression exxample.
\label{fig:regression}
}
\end{figure}

\subsection{Finite-difference approximation and the method of lines}

The finite-difference approximation is typically used for spatial derivatives.
Since most spatial problems are boundary-value problems (which implies second-order derivatives appear in governing equation), we can think of the solution as being interpolated\footnote{I'm using the term interpolate and extrapolate loosely.} between the two well-specified boundary points.
This is as opposed to initial-value problems, covered in Section~\ref{sec:num-ode}, in which only a single point is known (the initial value) and the answer is extrapolated in time from this point.
Hence, the methods are a litle simpler---at least on paper---for boundary value problems.

The workhorse of this method is called the finite-difference approximation (FDA), in which the first and second spatial derivatives of a dependent variable\footnote{The dependent variable is typically something like concentration, temperature, or enthalpy.} $y$ are approximated as:

\[ \frac{dy}{dx} \approx \frac{y_{i+1} - y_{i-1}}{2 \Delta x}, \hspace{2em}
\frac{d^2 y}{dx^2} \approx \frac{y_{i+1} - 2 y_i + y_{i-1}}{\left( \Delta x \right)^2}
\]


\noindent
where $y_i \equiv y(x_i)$.
You can derive this graphically by equating derivatives to slopes (and second derivatives to slopes of slopes), or more rigorously starting from a Taylor series expansion.\footnote{Using the Taylor series gives the same result, but also allows you to make functional predictions of how quickly the error reduces as you shrink $\Delta x$.  }

\subsubsection{Boundary value problems} \label{sec:fda}

Here, we deal with ordinary differential equations (that is, with only one independent variable); the next section will deal with partials.
The procedure is nearly the same.

\begin{figure}
\centering
\includegraphics[width=0.9 \textwidth]{f/fda-heat-tr/drawing.pdf}
\caption{Heat transfer through the wall of a pipe; example of finite-difference approximation.
\label{fig:fda-heat-tr}
}
\end{figure}

In class, we showed a simple equation from heat transfer through the wall of a pipe---e.g., removing heat from a PFR; this system is shown in Figure~\ref{fig:fda-heat-tr}.
The system is governed by the differential equation below, with boundary conditions given in the following lines.

{
\newcommand{\ri}{r_\mathrm{A}}
\newcommand{\re}{r_\mathrm{B}}
\newcommand{\ti}{T^\infty}

\[  0 = k \frac{d}{dr} \left( r \frac{dT}{dr} \right)  \]

\[  T \big|_{r=\ri} = T_\mathrm{w}  \]

\[  0 = -k \frac{dT}{dr} \Big|_{r=\re} - h \left( T\Big|_{r=\re} - \ti\right) \]


\noindent
Here, we will outline the steps in solving this numerically.


\begin{enumerate}
\item \emph{Discretize the control volume.} That is, draw points that cover the space where you want to model the problem, these are shown on the right of Figure~\ref{fig:fda-heat-tr}.
Here, we'll draw $n$ points, but start at $0$ and end at $n-1$, to be compatible with how arrays are numbered in python.

\item \emph{Apply the FDA to the governing equation at a generic point $i$.}
We demonstrate this for our system.
The differential equation can be re-written as

\[ 0 = \frac{dT}{dr} + r \frac{d^2 T}{dr^2} \]	

\[ 0 =  \frac{T_{i+1} - T_{i-1}}{2 \Delta r} + \left( \ri + i \Delta r \right) \frac{T_{i+1} - 2 T_{i} + T_{i-1}}{(\Delta r)^2} \]

		\begin{equation}\label{eq:fda-of-ht} 0 =  T_{i+1} - T_{i-1}  + 2  \left(\frac{\ri}{\Delta r} + i \right) \left( T_{i+1} - 2 T_{i} + T_{i-1} \right) \end{equation}

This FDA equation is valid at all points $1 \le i \le n - 2$.
		Note that from the geometry of the problem, $r_i$ was replaced with $(\ri + i \Delta r)$.

\item \emph{Apply the FDA to the boundary conditions.}
	Note that if we plugged $i=0$ into equation~\eqref{eq:fda-of-ht} we would get a $T_{-1}$ term, which has no meaning; similarly, $i=n-1$ would lead to a $T_{n}$ term.
		The two boundary conditions are fundamentally different\footnote{The first is called a ``Dirichlet'' condition and the second is called a ``Neumann''.} and we deal with them individually.
\begin{enumerate}
	\item \emph{Dirichlet.} The boundary condition at $\ri$ is called a ``Dirichlet'', which directly set the value of $y$ (not $dy/dt$).
		This is simple to deal with, we just add the equation below for point $i=0$:

		\[ T_0 = T_\mathrm{w} \]	

	\item \emph{Neumann/Robin.} The second boundary condition at $\re$ is in the category of ``Neumann'' or ``Robin''\footnote{Neumann set the value of the derivative, while Robin are a mix of Dirichlet and Neumann. Technically this one is a Robin. From out standpoint, Neumann and Robin are treated the same.}
		Here, we start by discretizing the boundary condition and finding the fictional value at $T_n$:

\[ 0 = - k \frac{T_{n} - T_{n-2}}{2 \Delta r} - h (T_{n-1} - \ti) \]

\[ T_n = T_{n-2} - \frac{2 h \Delta r}{k} \left(T_{n-1} - \ti \right) \]

		We can then use equation~\eqref{eq:fda-of-ht}, plugging in $i=n-1$, and we have the value to use for $T_n$.


\end{enumerate}

\item \emph{Assemble the whole system.}
We have now converted our differential equation into a set of coupled algebraic equations ($n$ equations and $n$ unknowns), of the form $\underline{0} = \underline{f}$.
These equations are listed below.

For $i=0$,

\[ f_0 =  T_0 - T_\mathrm{w} = 0 \]

For $1 \le i \le n - 2$,

\[ f_i =  \frac{1}{2} \left( T_{i+1} - T_{i-1} \right) +  (\frac{\ri}{\Delta r} + i) \left( T_{i+1} - 2 T_{i} + T_{i-1} \right) = 0 \]

For $i = n - 1$:


\[ f_{n-1} = \frac{1}{2} \left(T_{n} - T_{n-2}\right)+  \re \left(T_{n} - 2 T_{n-1} + T_{n-2}\right) = 0
\]

\noindent
(where $T_n$ is specified by:)

\[ T_n = T_{n-2} - \frac{2 h \Delta r}{k} \left(T_{n-1} - \ti \right) \]

\item \emph{Solve the system.}
Now we just solve for the values of $T_i$.
In some cases, it can make sense to do this by hand, but you typically use a numerical solver.\footnote{Such as scipy.optimize.root.}
Note that for algebraic systems in general, multiple solutions are often possible, and which solution you find is dictated by your initial guess (of $\underline{T}$, in this case).
Here, a good initial guess is to interpolate between $T_\mathrm{w}$ and $\ti$; however, this is not always so obvious.

\end{enumerate}

The solution to this problem is shown in Figure~\ref{fig:fda-heat-tr-fda}(left), made with the code below.
The parameters were put in to approximate a steel pipe with interior temperature of 400~K and ambient temperature of 300~K.
We can see the temperature profile is roughly constant, and close to the interior temperature; this matches intuition of a metal pipe with hot water running through it---it will be very hot to the touch.
\pcode{f/fda-heat-tr/run-fda.py}

\begin{figure}
\centering
\includegraphics[width=0.49 \textwidth]{f/fda-heat-tr/out-fda.pdf}
\includegraphics[width=0.49 \textwidth]{f/fda-heat-tr/out-mol.pdf}
\caption{ (Left) Steady-state solution to the heat conduction/convection problem through a pipe wall; that is, the solution to the boundary value problem.
	(Right) Transient solution to the same problem; that is, the solution to the initial boundary value problem.
\label{fig:fda-heat-tr-fda}
}
\end{figure}


\subsubsection{Initial Boundary Value Problems}

An initial boundary value problem is a type of partial differential equation often encountered in reactions and transport classes.
Typically, one independent variable will be time (which contains first derivative terms only) and the other independent variable will be a spatial coordinate (which contains both first and second derivatives).
The numerical solution to these sorts of problems, known as the \emph{method of lines}, is a straightforward combination of the finite-difference approximation (Section~\ref{sec:fda}) and numerical integration (Section~\ref{sec:num-ode}).

To solve this, we'll use the same system as above, but write the transient (rather than steady-state) equations that govern this heat transfer problem.
These transient equations are:


\[ \frac{\partial T}{\partial t} = \frac{k}{\rho C_P} \left( \frac{1}{r} \frac{\partial T}{\partial r} + \frac{\partial^2 T}{\partial r^2} \right)
\]

\noindent
We'll set the initial condition that the temperature is uniform at the ambient temperature:

\[\text{at } t=0,  T = \ti,  \ri \le r \le \re
\]

\noindent
The boundary conditions are the same as in the previous, but are valid at $t>0$ and are properly written as partial derivatives:

\[  T \big|_{r=\ri} = T_\mathrm{w}  \]

\[  0 = -k \frac{\partial T}{\partial r} \Big|_{r=\re} - h \left( T\Big|_{r=\re} - \ti\right) \]

The solution procedure feels the same as for the boundary value problem, the only difference being that we leave the time derivatives intact, which will allow us to convert our single partial differential equation into a system of ordinary differential equations, which are integrated over time to give the temperature profile over time.
This numerical integration uses an off-the-shelf routine, like \verb'scipy.integrate.odeint'.

The steps are written below for this example problem; note they only vary subtly from the previous example.

\begin{enumerate}
\item \emph{Discretize the control volume.} That is, draw points that cover the space where you want to model the problem, these are shown on the right of Figure~\ref{fig:fda-heat-tr}.
Here, we'll draw $n$ points, but start at $0$ and end at $n-1$, to be compatible with how arrays are numbered in python.

\item \emph{Apply the FDA to the governing equation at a generic point $i$.}
In our example, we'll apply the FDA to the spatial derivatives, which will turn this into a system of ODEs, rather than a PDE.
The PDE can be re-written as

\begin{equation}\label{eq:mol-of-ht}
\frac{d T_i}{dt} = \frac{k}{\rho C_P} \left( \frac{1}{\ri + i \Delta r}  \frac{T_{i+1} - T_{i-1}}{2 \Delta r} + \frac{T_{i+1} - 2 T_i + T_{i-1}}{(\Delta r)^2} \right)
\end{equation}

This FDA equation is valid at all points $1 \le i \le n - 2$.
Note that from the geometry of the problem, $r_i$ was replaced with $(\ri + i \Delta r)$.

\item \emph{Apply the FDA to the boundary conditions.}
	Note that if we plugged $i=0$ into equation~\eqref{eq:fda-of-ht} we would get a $T_{-1}$ term, which has no meaning; similarly, $i=n-1$ would lead to a $T_{n}$ term.
		The two boundary conditions are fundamentally different\footnote{The first is called a ``Dirichlet'' condition and the second is called a ``Neumann''.} and we deal with them individually.
\begin{enumerate}
\item \emph{Dirichlet.}
The Dirichlet boundary condition at $\ri$ directly sets the value of $y$ (not $dy/dt$).
That is, $T_0 = T_\mathrm{w}$ at $t > 0$.
There are two ways we can deal with this, and you can do whichever is more convenient.
One approach is to remove the differential equation for $dT_0/dt$ from the system, and just manually include its value in the final set of equations, since it is constant.
The second approach is to include $dT_0/dt = 0$ as the differential equation for this point, and set the value of $T_0$ to be $T_w$ right in the initial condition.
These will not give a meaningful difference in the results.

\item \emph{Neumann/Robin.} The second boundary condition at $\re$ is in the category of ``Neumann'' or ``Robin''\footnote{Neumann set the value of the derivative, while Robin are a mix of Dirichlet and Neumann. Technically this one is a Robin. From out standpoint, Neumann and Robin are treated the same.}
		Here, we start by discretizing the boundary condition and finding the fictional value at $T_n$:

\[ 0 = - k \frac{T_{n} - T_{n-2}}{2 \Delta r} - h (T_{n-1} - \ti) \]

\[ T_n = T_{n-2} - \frac{2 h \Delta r}{k} \left(T_{n-1} - \ti \right) \]

		We can then use equation~\eqref{eq:mol-of-ht}, plugging in $i=n-1$, and we have the value to use for $T_n$.


\end{enumerate}

\item \emph{Assemble the whole system.}
We have now converted our partial differential equation into a system of ordinary differential equation equations, of the form $d \underline{y}/dt = \underline{f}$.
These equations are listed below.

For $i=0$, we'll take the option of including $dT_0/dt=0$ in our system of ODEs, as its a bit simpler to code since we don't have to worry about re-numbering our points.

\[ f_0 = 0 \]

For $1 \le i \le n - 2$,

\[ f_i = \frac{k}{\rho C_P} \left( \frac{1}{\ri + i \Delta r}  \frac{T_{i+1} - T_{i-1}}{2 \Delta r} + \frac{T_{i+1} - 2 T_i + T_{i-1}}{(\Delta r)^2} \right) \]

For $i = n - 1$:

\[ f_{n-1} = \frac{k}{\rho C_P} \left( \frac{1}{\re}  \frac{T_{n} - T_{n-2}}{2 \Delta r} + \frac{T_{n} - 2 T_{n-1} + T_{n-1}}{(\Delta r)^2} \right) \]

\noindent
(where $T_n$ is specified by:)

\[ T_n = T_{n-2} - \frac{2 h \Delta r}{k} \left(T_{n-1} - \ti \right) \]

\item \emph{Integrate the system.}
Now we just integrate the system of equations, using a numerical solver.\footnote{Such as scipy.integrate.odeint}

\end{enumerate}

The solution to this problem is shown in Figure~\ref{fig:fda-heat-tr-fda}(right), made with the code below.
We can see the temperature profile evolving over time from the initially flat profile.
Note the one odd point at the beginning, where the left-most point is already at $T_\mathrm{w}$ before the integration started; this is due to our choice of how to handle the Dirichlet condition.
\pcode{f/fda-heat-tr/run-mol.py}


} % end FDA example commands

\section{Stability of steady-state solutions}
{ % stability commands
\newcommand{\m}[1]{\ensuremath{\mathbf{\underline{\underline{#1}}}}}
\renewcommand{\v}[1]{\ensuremath{\mathbf{\underline{#1}}}}
\newcommand{\bm}[1]{\begin{bmatrix} #1 \end{bmatrix}}
\newcommand{\norm}[1]{\left\lVert #1 \right\rVert}
\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}
\newcommand{\yss}{\v{y}_\mathrm{SS}}

Often in reactors, we find situations with multiple steady states possible.
First, recall that a steady state solution is one in which the time derivatives are zero; $d\v{y}/dt = \v{0}$.
The algebraic equations that result from this this condition can have multiple roots.
However, it is often the case that not all of these solutions are stable.
Certainly, if you integrate $d\v{y}/dt$ starting from any of the roots $\yss$, you will just get $\yss$ at all times $t>0$.
However, when we discuss stability we ask what happens if we perturb the system slightly; that is, if we integrate from $\yss + \v{\epsilon}$ instead, where $\v{\epsilon}$ is a small number.
Does it return to $\yss$ or go somewhere else?

For the particular case of a CSTR, we can use generation and removal curves to analyze this graphically, gaining intuition about the operation.
However, here we instead provide a general technique to analyze this problem, which can be applied to any ODE system.

\subsection{Derivation}

We provide a brief, but complete derivation below.
First, we want to analyze the system:
\[ \frac{d \v{y}}{dt} = \v{f} \]
\noindent
for the stability of the steady state solutions $\yss$, which were found from $\v{f} = \v{0}$.
To do so, we want to examine the behavior of the system for small perturbations $\v{\epsilon}$ from the steady-state solution:

\[ \v{y} = \yss + \v{\epsilon} \]

\noindent
and we approximate our function $\v{f}$ with a Taylors series expansion about $\yss$.
Note that the Jacobian matrix $\m{J}$ has elements $\partial f_i / \partial y_j$, each evaluated at $\yss$.

\[ \v{f} \approx \m{J}\Big|_{\yss} \v{\epsilon} \]

\noindent
We plug these into our original equation, which simplifies since $d\yss/dt = \v{0}$ and find

\[ \frac{d \v{\epsilon}}{dt} \approx \m{J}\Big|_{\yss} \v{\epsilon} \]

\noindent
and now we have a linear system of ODEs that we learned how to analyze in class.
That is, the general solution to this differential equation is:

\[ \v{\epsilon}(t) = c_1 \v{k}_1 e^{\lambda_1 t} + c_2 \v{k}_2 e^{\lambda_2 t} + \cdots + c_n \v{k}_n e^{\lambda_n t} \]

\noindent
where $\lambda_i$ and $\v{k}_i$ are the eigenvalues and eigenvectors of $\m{J}$, and $c_i$ are constants specified from the initial value problem.
Clearly, only if all values of $\lambda_i$ are less than zero will $\v{\epsilon}$ decay back to zero (that is, $\v{y}$ returns to $\yss$).
Additionally, if any eigenvalues are complex, the imaginary component will cause oscillations.\footnote{By Euler's formula: $e^{it} = \cos t + i \sin t$.}



\subsection{Recipe}

Here, we provide a recipe for using this technique to analyze the stability of steady-state solutions.

\begin{enumerate}

\item Write the system of ODEs.
\[ \frac{d \v{y}}{dt} = \v{f}(\v{y}, t) \]

\item Find the steady state of interest. E.g., by setting derivatives to zero and doing algebra.

\item Write the Jacobian matrix, that is,

\[ \m{J} =  [J_{ij}] = [\frac{\partial f_i}{\partial y_j}]\]

Evaluate this at $\yss$.

\item Find the eigenvalues $\left\{ \lambda \right\}$ of the Jacobian.
	For systems of size $2 \times 2$ or less, a hand solution is reasonable; for larger systems, use something like \verb'numpy.linalg.eig'.

\item The stability can be determined from the eigenvalues, where $\mathrm{Re}$ and $\mathrm{Im}$ indicate the real and complex parts of the eigenvalues.

\begin{itemize}
\item All $\mathrm{Re}(\lambda) < 0$: stable
\item Any $\mathrm{Re}(\lambda) > 0$: unstable
\item Any $\mathrm{Im}(\lambda) \ne 0$: oscillates
\end{itemize}
\end{enumerate}

\subsection{Numerical example}

Here, we provide a simple example; this example comes from the textbook by Beers~\cite{Beers2006} in which two steady-state roots are sequentially explored.
Consider the system:

\begin{align*}
\frac{dy_1}{dt} &= y_1^2 +  y_1 - y_2 - 1 = f_1 \\
\frac{dy_2}{dt} &= -y_1 - 3 y_2^2 + 2 y_2 + 2 = f_2
\end{align*}

\noindent
We can find a steady-state solution at (1,1).
Let's first examine this solution before looking for others.
We write the Jacobian and plug in $\yss$:

\[ \m{J} = \bm{ 2 y_1 + 1 & -1 \\ -1 & -6 y_2 + 2} = \bm{3 & -1 \\ -1 & -4 } \]

\noindent
The eigenvalues of this matrix are 3.1, -4.1; therefore, this is an unsteady solution.
We can plot this to see how it looks; this is shown in Figure~\ref{fig:stability}.
Interestingly, we see that if we numerically integrate our system from $\v{\epsilon}$ = (-0.1, 0.1) we find a stable steady-state solution.
(We could then examine that root with a similar analysis.)
If we numerically integrate from (0.1, 0.1) we appear to diverge into numerical instability.

\begin{figure}
\centering
\includegraphics[width=0.99 \textwidth]{f/stability/out.pdf}
\caption{Numerically integrating from an unstable root; note the behavior is quite different depending on the direction of the perturbation.
\label{fig:stability}
}
\end{figure}

\pcode{f/stability/run.py}





} % end stability commands


\bibliographystyle{unsrt}

\bibliography{/home/aap/Dropbox/all_literature}
\end{document}

% Revision notes:
